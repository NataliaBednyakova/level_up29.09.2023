/*let salaries = { 
    John: 100, 
    Ann: 160, 
    Pete: 130, 
    Alex: 170 };

let sum = 0;

for(let value in salaries){
    sum += salaries[value];
};

console.log(sum);


let age = 91;

if (14 <= age && age <= 90){
    console.log('You can come in!');
}else{
    console.log('Sorry, but you cannot come in.');
}*/

//--------------------------------------------------------------

//2 works

/*function getPlayersNewHealth(currentHealth, traumas) {
    let newHealth = currentHealth-traumas;
    if(newHealth > 0){
        console.log(newHealth);
    }else{
        console.log("Health cannot go below 0");
    }
}

getPlayersNewHealth(100, 5);
getPlayersNewHealth(92, 8);
getPlayersNewHealth(20, 30);*/



//3 works

/*function changeToNumber(str){
    console.log(Number(str));
}

changeToNumber('1234');
changeToNumber('605');
changeToNumber('1405');
changeToNumber('-7');

let changeToNumber2 = (str) => console.log(+str);
changeToNumber2('1234');
changeToNumber2('605');
changeToNumber2('1405');
changeToNumber2('-7');*/



//4 Works 

/*let arr1 = [1, 1, 2];
let arr2 = [1, 2, 1, 1, 3, 2];

function removeDupes(arr){
    const seenValues = {};
    let newArr = [];
    let j = 0;
    for(let i = 0; i < arr.length; i++){

        const itemType = typeof(arr[i]);
        const key = `${itemType}_${arr[i]}`;
        if(!seenValues[key]){
            seenValues[key] = 1;
            newArr[j++] = arr[i];
        }
    }
    console.log(newArr)
}

removeDupes(arr1);
removeDupes(arr2);*/



//5 works

/*let strObj = '45385593107843568';
let strObj2 = '509321967506747';
let strObj3 = '366058562030849490134388085';


function convertToBinary(str){
    let newStr = '';
    for(let i = 0; i < str.length; i++){
        if(str[i] < 5){
            newStr+=0;
        }else{
            newStr+=1;
        }
    }
    console.log(newStr);
}

convertToBinary(strObj);
convertToBinary(strObj2);
convertToBinary(strObj3);*/

//-----------------------------------------------------------

//1 works

/*function filterArray (arr){
    let newArr = [];
    for(let i = 0; i < arr.length; i++){
        if(arr[i] === Number(arr[i])){
            newArr.push(arr[i]);
        }
    }
    console.log(newArr);
};

filterArray([1, 2, "a", "b"]);
filterArray([1, "a", "b", 0, 15]);
filterArray([1, 2, "aasf", "1", "123", 123]);*/


//2 works

/*let formatPhoneNumber = (arr) => {
    let arrStr = arr.join('');
    console.log(`(${arrStr.slice([0], [3])})${arrStr.slice([3], [6])}-${arrStr.slice([6],[10])}`);
}

formatPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);
formatPhoneNumber([5, 1, 9, 5, 5, 5, 4, 4, 6, 8]);
formatPhoneNumber([3, 4, 5, 5, 0, 1, 2, 5, 2, 7]);*/


//3 works

/*function evenOrOdd(arr){
    let sum = 0;
    for(let i = 0; i < arr.length; i++){
        sum+=arr[i];
    }
    if(sum%2==0){
        console.log('Even');
    }else if(sum%2!=0){
        console.log('Odd');
    }
}

evenOrOdd([0]);
evenOrOdd([1]); 
evenOrOdd([]);
evenOrOdd([0, 1, 5]); 
evenOrOdd([1, 6, 8, 3, 0]);*/


//4 works 

/*function volumeOftheBox({width, length, height}){
    let volume = width*length*height;
    console.log(volume);

}

function volumeOftheBox({width, length, height}){
    const params = {
        width,
        length,
        height
    };

    let volume = 1;
    for(let prop in params){
        volume *= params[prop];
        
    }
    console.log(volume);
}

volumeOftheBox({ width: 2, length: 5, height: 1 });
volumeOftheBox({ width: 4, length: 2, height: 2 });
volumeOftheBox({ width: 2, length: 3, height: 5 });*/


//5 doesn't work

/*let oldest = function({...name}){
    const human = {
        ...name,
    };

    let newOldest = [];
    let greatAge;

    for(let key in human){
        newOldest.push([key, human[key]]);
        if(newOldest[key] == newOldest[key+1]){
            console.log(newOldest);
        }


    }
    //console.log(newOldest)
}

oldest({ Emma: 71, Jack: 45, Amy: 15, Ben: 29 });
oldest({ Max: 9, Josh: 13, Sam: 48, Anne: 33 });*/



//6 works

/*let dnaToRna = (arg) =>{
    let RNA = '';
    for(let i = 0; i < arg.length; i++){
        switch(arg[i]){
            case 'A':
                RNA+='U';
                break;
            case 'T':
                RNA+='A';
                break;
            case 'G':
                RNA+='C';
                break;
            case 'C':
                RNA+='G';
                break;
        }
    }
    console.log(RNA)
}

dnaToRna("ATTAGCGCGATATACGCGTAC");
dnaToRna("CGATATA");
dnaToRna("GTCATACGACGTA");*/


//7 works

/*const perimeter = function(figure, num){
    let result = figure == 's' ? 4*num : figure == 'c' ? 6.28*num : 'No such figure';
    console.log(result);
}

perimeter('s', 7);
perimeter('c', 4);
perimeter('c', 9);*/


//8 works

/*function hackerSpeak (str){
    str = str.split('');
    //console.log(str);
    for(let i = 0; i < str.length; i++) {
        //console.log(i);
        /*if(str[i] == 'a'){
            str[i] = 4;
        }else if(str[i] == 'e'){
            str[i] = 3;
        }else if(str[i] == 'i'){
            str[i] = 1;
        }else if(str[i] == 'o'){
            str[i] = 0;
        }else if(str[i] == 's'){
            str[i] = 5;
        }

        switch(str[i]){
            case 'a':
                str[i] = 4;
                break;
            case 'e':
                str[i] = 3;
                break;
            case 'i':
                str[i] = 1;
                break;
            case 'o':
                str[i] = 0;
                break;
            case 's':
                 str[i] = 5;
                 break;
        }
    }
    str = str.join('');
    console.log(str);
}

hackerSpeak("javascript is cool");
hackerSpeak("programming is fun");
hackerSpeak("become a coder");*/


//9 works

/*let findNaN = function(arr){
    for(let i = 0; i < arr.length; i++){
        if(isNaN(arr[i])){
            console.log(i);
        }else if(arr[i] == !isNaN){
            console.log(-1);
        };
    }
}



findNaN([1, 2, NaN]);
findNaN([NaN, 1, 2, 3, 4]);
findNaN([0, 1, 2, 3, 4]);*/


//----------------------------------------------------------------------------

const nums = [1, 2, 3, 4, 5, 6];
const strs = ['Hello', 'how', 'are', 'you', 'doing?']

// -----------------map-----------------

Array.prototype.customMap = function(callback){
    let newArr = [];
    for(let i = 0; i < this.length; i++){
        let newMapItem = callback(this[i], i, this)
        newArr.push(newMapItem);
    }
    return newArr;
}

let newNums = nums.map((num) => num*2);
console.log(newNums);

let newNums2 = nums.customMap((num) => num*2);
console.log(newNums2);

let newStr = strs.customMap((str) => str.length);
console.log(newStr);

let newStr2 = strs.customMap((str) => str.length);
console.log(newStr2);

//--------------------filter---------------------------

Array.prototype.customfilter = function(callback){
    let newResult = [];
    for(let i = 0; i < this.length; i++){
        let matchArr = callback(this[i], i, this);
        if(matchArr){
            newResult.push(this[i]);
        }else{
            continue;
        }
    }
    return newResult;
}

let evenNums = nums.filter((num) => num%2 == 0);
console.log(evenNums);

let evenNums2 = nums.customfilter((num) => num%2 == 0);
console.log(evenNums2);

let threeLetters = strs.filter((str) => str.length <= 3);
console.log(threeLetters);

let threeLetters2 = strs.customfilter((str) => str.length <= 3);
console.log(threeLetters2);

//----------------------------------------------------------------